var gulp = require('gulp');
var sass = require('gulp-sass');
sourcemaps = require('gulp-sourcemaps');

gulp.task('watch', function() {
    gulp.watch(['/styles/' + '**/*.scss'], ['sass']);
});


var browserSync = require('browser-sync').create();
gulp.task('dev', ['sass'], function() { 
   browserSync.init({
    server: {
        baseDir: "./"
    }
});  
    gulp.watch('styles/**/*.scss', ['sass']);  
    gulp.watch(  "**/*.html").on('change', browserSync.reload); 
});


gulp.task('styles', function () {
    return sass('styles/style.scss', { style: 'compressed', sourcemap:true })
      .pipe(sourcemaps.init())
      .pipe(autoprefixer(autoprefixerOptions))
      .pipe(sourcemaps.write())
      .pipe(sourcemaps.write('maps', {
        includeContent: false,
        sourceRoot: 'source'
      }))
      .pipe(gulp.dest(output))
      .pipe(browserSync.stream({ match: '**/*.css' }));
  });


gulp.task('sass', function () {
    return gulp.src([
     "./styles/" + 'style.scss'
    ])
     .pipe(sass({outputStyle: 'compact'}))
     .pipe(gulp.dest('./'))
        .pipe(browserSync.stream())
    });